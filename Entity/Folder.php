<?php

namespace BDSA\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Folder
 * 
 * @ORM\Table(name="bdsa_media_folder")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="BDSA\MediaBundle\Repository\FolderRepository")
 */

class Folder
{
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
	private $name;

	/**
    * @ORM\ManyToOne(targetEntity="Folder", inversedBy="children")
    * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
    */
	private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Folder", mappedBy="parent", cascade={"remove"})
     */
    private $children;

    public function __construct()
    {
        $this->folders = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Folder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parent
     *
     * @param \BDSA\MediaBundle\Entity\Folder $parent
     *
     * @return Folder
     */
    public function setParent(\BDSA\MediaBundle\Entity\Folder $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \BDSA\MediaBundle\Entity\Folder
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function addChild(\BDSA\MediaBundle\Entity\Folder $child)
    {
        $this->children[] = $child;

        return $this;
    }

    public function removeChild(\BDSA\MediaBundle\Entity\Folder $child)
    {
        $this->children->removeElement($child);
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function hasChildren()
    {
        return 0 !== count($this->children);
    }
}
