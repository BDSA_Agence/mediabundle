<?php

namespace BDSA\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Document
 * 
 * @ORM\Table(name="bdsa_media_document")
 * @ORM\Entity(repositoryClass="BDSA\MediaBundle\Repository\DocumentRepository")
 */

class Document
{
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
	private $name;

	/**
     * @var text
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
	private $description;

	/**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255)
     * @Assert\File()
     */
	private $file;

	/**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=255)
     */
	private $extension;

	/**
     * @var string
     *
     * @ORM\Column(name="mimetype", type="string", length=255)
     */
	private $mimetype;

	/**
    * @ORM\ManyToOne(targetEntity="Folder")
    * @ORM\JoinColumn(name="folder_id", referencedColumnName="id", onDelete="cascade")
    */
	private $folder;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=255)
     */
    private $size;

	/**
     * @var bool
     * 
     * @ORM\Column(name="is_trashed", type="boolean")
     */
	private $is_trashed = false;

	/**
     * @var datetime
     *
     * @ORM\Column(name="date_upload", type="datetime")
     */
	private $date_upload;

	/**
     * @var datetime
     *
     * @ORM\Column(name="date_edit", type="datetime")
     */
	private $date_edit;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Document
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Document
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return Document
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set mimetype
     *
     * @param string $mimetype
     *
     * @return Document
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    /**
     * Get mimetype
     *
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Set isTrashed
     *
     * @param boolean $isTrashed
     *
     * @return Document
     */
    public function setIsTrashed($isTrashed)
    {
        $this->is_trashed = $isTrashed;

        return $this;
    }

    /**
     * Get isTrashed
     *
     * @return boolean
     */
    public function getIsTrashed()
    {
        return $this->is_trashed;
    }

    /**
     * Set dateUpload
     *
     * @param \DateTime $dateUpload
     *
     * @return Document
     */
    public function setDateUpload($dateUpload)
    {
        $this->date_upload = $dateUpload;

        return $this;
    }

    /**
     * Get dateUpload
     *
     * @return \DateTime
     */
    public function getDateUpload()
    {
        return $this->date_upload;
    }

    /**
     * Set dateEdit
     *
     * @param \DateTime $dateEdit
     *
     * @return Document
     */
    public function setDateEdit($dateEdit)
    {
        $this->date_edit = $dateEdit;

        return $this;
    }

    /**
     * Get dateEdit
     *
     * @return \DateTime
     */
    public function getDateEdit()
    {
        return $this->date_edit;
    }

    /**
     * Set folder
     *
     * @param \BDSA\MediaBundle\Entity\Folder $folder
     *
     * @return Document
     */
    public function setFolder(\BDSA\MediaBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return \BDSA\MediaBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return Document
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if( empty($this->getDateUpload()) & empty($this->getFile()) )
        {
            $context->buildViolation('BDSA.media.document.file.cannot_be_null')
                ->atPath('file')
                ->addViolation();
        }
    }
}
