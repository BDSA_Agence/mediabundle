<?php

namespace BDSA\MediaBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $file)
    {
        $file_infos = array();

        $name      = $file->getClientOriginalName();
        $extension = $file->guessExtension();
        $mimetype  = $file->getMimeType();
        $size      = $file->getClientSize();
        $filename  = md5(uniqid()).'.'.$extension;

        $file->move($this->getTargetDir(), $filename);

        $file_infos['name']      = $name;
        $file_infos['filename']  = $filename;
        $file_infos['extension'] = $extension;
        $file_infos['size']      = $size;
        $file_infos['mimetype']  = $mimetype;

        return $file_infos;
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
}