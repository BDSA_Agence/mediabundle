<?php

namespace BDSA\MediaBundle\Controller;

use BDSA\MediaBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Document controller.
 *
 */
class DocumentController extends Controller
{
    /**
     * Lists all document entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $documents = $em->getRepository('BDSAMediaBundle:Document')->findAll();

        return $this->render('BDSAMediaBundle:document:index.html.twig', array(
            'documents' => $documents,
        ));
    }

    /**
     * Creates a new document entity.
     *
     */
    public function newAction(Request $request)
    {
        $document = new Document();
        $form = $this->createForm('BDSA\MediaBundle\Form\DocumentType', $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();

            if( empty($document->getFolder()) )
                return $this->redirectToRoute('bdsa_folder_index');
            else
                return $this->redirectToRoute('bdsa_folder_show', array('id' => $document->getFolder()));
        }

        return $this->render('BDSAMediaBundle:document:new.html.twig', array(
            'document' => $document,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a document entity.
     *
     */
    public function showAction(Document $document)
    {
        $deleteForm = $this->createDeleteForm($document);

        return $this->render('BDSAMediaBundle:document:show.html.twig', array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing document entity.
     *
     */
    public function editAction(Request $request, Document $document)
    {
        $document->setFile(
            new File($this->getParameter('bdsa_media_upload_dir').$document->getFile())
        );
        
        $deleteForm = $this->createDeleteForm($document);

        $editForm = $this->createForm('BDSA\MediaBundle\Form\DocumentType', $document);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $original = $this->getDoctrine()->getManager()->getUnitOfWork()->getOriginalEntityData($document);

            /*print_r($original);
            echo '<br/>';
            echo $this->getParameter('BDSA_media_upload_dir').$original['file'];
            die('---');*/

            if( empty($document->getFile()) )
            {
                $document->setFile(
                    $original['file']
                );
            }

            $this->getDoctrine()->getManager()->flush();

            if( empty($document->getFolder()) )
                return $this->redirectToRoute('bdsa_folder_index');
            else
                return $this->redirectToRoute('bdsa_folder_show', array('id' => $document->getFolder()->getId()));
        }

        return $this->render('BDSAMediaBundle:document:edit.html.twig', array(
            'document' => $document,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function downloadAction(Request $request, Document $document)
    {
        $slugger = $this->container->get('BDSA.media.slugger');

        $response = new BinaryFileResponse($this->get('kernel')->getRootDir().'/../web/uploads/'.$document->getFile());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $slugger->slugify($document->getName()).'.'.$document->getExtension()
        );

        //$dispatcher->dispatch(BDSAPlatformEvent::DOWNLOAD_DOCUMENT, $event);

        return $response;
    }

    /**
     * Deletes a document entity.
     *
     */
    public function deleteAction(Request $request, Document $document)
    {
        $form = $this->createDeleteForm($document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($document);
            $em->flush();
        }

        return $this->redirectToRoute('bdsa_document_index');
    }

    /**
     * Creates a form to delete a document entity.
     *
     * @param Document $document The document entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Document $document)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bdsa_document_delete', array('id' => $document->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
