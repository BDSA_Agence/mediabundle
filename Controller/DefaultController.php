<?php

namespace BDSA\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BDSAMediaBundle:Default:index.html.twig');
    }
}
