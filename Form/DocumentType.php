<?php

namespace BDSA\MediaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use BDSA\MediaBundle\Repository\FolderRepository;

class DocumentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', TextType::class, array('label' => 'bdsa.media.document.name'))
        ->add('description', TextareaType::class, array('label' => 'bdsa.media.document.description', 'required' => false))
        ->add('file', FileType::class, array('label' => 'bdsa.media.document.file', 'required' => false, 'mapped' => true))
        ->add('folder',
            EntityType::class,
            array(
                'label' => 'bdsa.media.document.folder',
                'class' => 'BDSAMediaBundle:Folder',
                'required'   => false,
                'placeholder' => 'bdsa.media.folder.root',
                'query_builder' => function (FolderRepository $er) {
                    return $er->createQueryBuilder('f')->orderBy('f.name', 'ASC');
                },
            )
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BDSA\MediaBundle\Entity\Document'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bdsa_mediabundle_document';
    }


}
