<?php

namespace BDSA\MediaBundle\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

use BDSA\MediaBundle\Entity\Document;
use BDSA\MediaBundle\Service\FileUploader;

class DocumentUploadListener
{
    private $uploader;
    private $targetDir;

    public function __construct(FileUploader $uploader, $targetDir)
    {
        $this->uploader  = $uploader;
        $this->targetDir = $targetDir;
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if( !$entity instanceof Document )
            return;

        $fs = new Filesystem();
        $fs->remove(array( $this->targetDir.'/'.$entity->getFile() ));
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if( !$entity instanceof Document )
            return;

        $entity->setDateUpload( new \DateTime() );
        $entity->setDateEdit( new \DateTime() );

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        if( !$entity instanceof Document )
            return;

        $entity->setDateEdit( new \DateTime() );

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        if (!$entity instanceof Document) {
            return;
        }

        $fs = new Filesystem();

        if( !$fs->exists( $this->targetDir ) )
            $fs->mkdir( $this->targetDir, 0755 );

        $file = $entity->getFile();

        if( $file instanceof UploadedFile )
        {
            $file_infos = $this->uploader->upload($file);

            if( empty($entity->getDateUpload()) )
                $entity->setName($file_infos['name']);
            $entity->setExtension($file_infos['extension']);
            $entity->setMimetype($file_infos['mimetype']);
            $entity->setFile($file_infos['filename']);
            $entity->setSize($file_infos['size']);
        }
    }
}