# BDSA\MediaBundle

[![Build Status](https://travis-ci.org/BDSA/MediaBundle.svg?branch=master)](https://travis-ci.org/BDSA/MediaBundle)

## Add routing to routing.yml

```yml
bdsa_media:
    resource: "@BDSAUserBundle/Resources/config/routing.yml"
```

## Default Configuration for your config.yml

Basically use `php bin/console config:dump-reference bdsa_media` to dump the default configuration.

```yml
paramaters:
    # ...
    bdsa_media_upload_dir: 'uploads/'

# ...


# Default configuration for extension with alias: "bdsa_media"
bdsa_media:
    upload_dir: '%kernel.project_dir%/web/%bdsa_media_upload_dir%'
```

`php bin/console doctrine:schema:validate`
`php bin/console doctrine:schema:update --force`

[BDSA](http://bdsa.fr)